import image1 from '../src/assets/pro1.jpg'
import image2 from '../src/assets/pro2.jpg'
import image3 from '../src/assets/pro3.jpg'
import image4 from '../src/assets/pro4.jpg'
import image5 from '../src/assets/pro5.jpg'
import image6 from '../src/assets/pro6.jpg'
import image7 from '../src/assets/pro7.jpg'
import image8 from '../src/assets/pro8.jpg'
import image9 from '../src/assets/hair1.jpg'
import image10 from '../src/assets/hair2.jpg'
import image11 from '../src/assets/hair3.jpg'
import image12 from '../src/assets/hair4.jpg'
import image13 from '../src/assets/hair5.jpg'
import image14 from '../src/assets/hair6.jpg'
import image15 from'../src/assets/body1.jpg'
import image16 from'../src/assets/body2.jpg'


export const products = [
    {
        id: 1,
        title: "Hyalorunic Acid 2% the ordinary",
        price: 28,
        description: "The Ordinary Hyaluronic Acid 2% + B5 - a hydration support formula with ultra-pure, vegan hyaluronic acid. Hyaluronic Acid (HA) can attract up to 1,000 times its weight in water. ",
        category: "skin care",
        rating: {"rate":3.9,"count":120},
        img : image1,

    },
    {
        id: 2,
        title: "Glycolique Acid 7%",
        price: 30,
        description: "Glycolic Acid is an alpha hydroxy acid that exfoliates the skin. This 7% toning solution offers mild exfoliation for improved skin radiance and visible clarity. ",
        category: "skin care",
        rating: {"rate":3.9,"count":120},
        img : image2,

    },
    
    {
        id: 3,
        title: "HA BHA peeling solution",
        price: 100,
        description: "The Ordinary is one of the best affordable skin care brands and their AHA BHA peeling solution gives results right away and get better with use! ",
        category: "skin care",
        rating: {"rate":3.9,"count":120},
        img : image3,

    },
    {
        id: 4,
        title: "Tha Ordinary Niacinamide",
        price: 23,
        description: "The Ordinary Niacinamide 10% + Zinc 1% serum effectively and explore its incredible benefits. ",
        category: "skin care",
        rating: {"rate":3.9,"count":120},
        img : image4,

    },
    
    {
        id: 5,
        title: "K-Beauty",
        price: 14,
        description: "blumby lips with k-beaty ",
        category: "skin care",
        rating: {"rate":3.9,"count":120},
        img : image5,

    },
    
    {
        id: 6,
        title: "Revolution",
        price: 17,
        description: "The perfect scrub you can use to your skin",
        category: "skin care",
        rating: {"rate":3.9,"count":120},
        img : image6,

    },
    
    {
        id: 7,
        title: "AMETHYST",
        price: 200,
        description: "SCRUB the dead skin from ur face  ",
        category: "skin care",
        rating: {"rate":3.9,"count":120},
        img : image7,

    },
    {
        id: 8,
        title: "Clean It ",
        price: 90,
        description: "Banila Co. - Clean It Zero Cleansing Balm Original Un baume nettoyant qui dissout parfaitement le maquillage du visage et des yeux, même lorsqu’il est résistant à l’eau. ",
        category: "skin care",
        rating: {"rate":3.9,"count":120},
        img : image8,

    },    {
        id: 9,
        title: "Scalp Massage",
        price: 100,
        description:"scalp the love and care it deserves! The GRO Scalp Revitalizing Massager features a series of bristles that stimulate the scalp and boost scalp helth",
        category: "hair care",
        rating: {"rate":3.9,"count":120},
        img : image9,

    },
    {
        id: 10,
        title: "OLAPLEX",
        price: 50,
        description: "OLAPLEX No.3 Hair Perfector - Repairing Treatment, 3.3oz ",
        category: "hair care",
        rating: {"rate":3.9,"count":120},
        img : image10,

    },
    {
        id: 11,
        title: "OUAI",
        price: 300,
        description: "Turn the moisture all the OUAI up with this rich mask for thick hair that delivers major moisture, damage repair, and shine in minutes. ",
        category: "hair care",
        rating: {"rate":3.9,"count":120},
        img : image11,

    },
    {
        id: 12,
        title: "Mielle",
        price: 40,
        description: "Mielle Organics Rosemary Mint Scalp & Hair Strengthening Oil With Biotin & Essential Oils, Nourishing Treatment for Split Ends and Dry Scalp for All Hair Types ",
        category: "hair care",
        rating: {"rate":3.9,"count":120},
        img : image12,

    },
    {
        id: 13,
        title: "Kerastase",
        price: 180,
        description: "Infused with Manuka Honey and Ceramides, Kerastase Curl Manifesto haircare range deeply nourishes and defines curly hair to leave them soft & bouncy! ",
        category: "hair care",
        rating: {"rate":3.9,"count":120},
        img : image13,

    },
    {
        id: 14,
        title: "Avocado oil",
        price: 200,
        description: "yAvocado oil is one of the best deeply nourishing oils for your hair. Find out 7 benefits of avocado oil for hair!",
        category: "hair care",
        rating: {"rate":3.9,"count":120},
        img : image14,

    },
    {
        id: 15,
        title: "Scrub body ",
        price: 42,
        description: "scrub your body gently with Dove",
        category: "body care",
        rating: {"rate":3.9,"count":120},
        img : image15,

    },
    {
        id: 16,
        title: "Lotion",
        price: 200,
        description: "body and cream for your body has good perfume ",
        category: "body care",
        rating: {"rate":3.9,"count":120},
        img : image16,

    },

    
    
    

]