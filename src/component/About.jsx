import React from 'react';
import styled from 'styled-components';


const About = () => {
  return (
    <StyledAboutPage>
      <h2>About Our Skincare Products</h2>
      <p>
        Welcome to our skincare haven! At <span className="highlight">HASSMERY</span>, we are passionate about
        providing you with high-quality skincare products that enhance the health and beauty of your skin.
      </p>
      <p>
        Our mission is to offer a curated selection of the finest skincare items, carefully chosen for their
        effectiveness and natural ingredients. We believe in the power of healthy, radiant skin, and we strive to
        empower you on your skincare journey.
      </p>
      <p>
        Whether you are looking for rejuvenating serums, nourishing creams, or refreshing cleansers, we have a diverse
        range of products to suit your unique needs. Our team of skincare enthusiasts is dedicated to bringing you the
        latest innovations and time-tested classics.
      </p>
      <p>
        Thank you for choosing <span className="highlight">HASSMERY</span> as your skincare destination. Explore
        our collection, and let your skin glow with vitality!
      </p>
    </StyledAboutPage>
  );
};
const StyledAboutPage = styled.div`
  max-width: 800px;
  margin: 0 auto;
  padding: 40px;
  text-align: justify;

  h2 {
    color: rgb(210, 120, 120);
    font-size: 28px;
    margin-bottom: 20px;
  }

  p {
    color: #555;
    font-size: 20px;
    line-height: 1.5;
  }

  .highlight {
    color: brown;
    font-weight: bold;
  }
`;

export default About;
