import React, { useState } from 'react';
import './Contact.css';
import { useNavigate } from 'react-router-dom';

function Contact() {
  const [nom, setNom] = useState("");
  const [tele, setTele] = useState("");
  const [email, setEmail] = useState("");
  const [theme, setTheme] = useState("");
  const [star, setStar] = useState("");
  const [isEditing, setIsEditing] = useState(true);
  const navigate = useNavigate();

  const IsValidate = () => {
    let isproceed = true;
    let errormessage = 'Please enter the value into';
    if (nom === null || nom === '') {
      isproceed = false;
      errormessage += " Nom & Prenom ";
    }
    if (tele === null || tele === '') {  
      isproceed = false;
      errormessage += " Thelephone ";
    }
    if (email === null || email === '') {
      isproceed = false;
      errormessage += " Email ";
    }
    if (!isproceed) {
      alert(errormessage);
    } else {
      if (/^[^\s@]+@[^\s@]+\.[^\s@]+$/.test(email)) {

      } else {
        isproceed = false;
        alert('Please enter the valid email');
      }
    }
    return isproceed;
  };

  const sendEmail = (e) => {
    e.preventDefault();
    const formInfo = {
      nom,
      tele,
      email,
      theme,
      star, 
    };
    if (IsValidate()) {
      fetch("http://localhost:8000/contact", {
        method: "POST",
        headers: { 'content-type': 'application/json' },
        body: JSON.stringify(formInfo)
      })
        .then((res) => {
          alert('successfully.');
          navigate('/home');
        })
        .catch((err) => {
          alert('Failed: ' + err.message);
        });
    }
  };

  return (
    <div className='contactForm' id='contact'>
      <form onSubmit={sendEmail}>
        <h1 className='titleContact'>Send us a message</h1>
        <div className="input_Contact">
          <input placeholder='Nom & Prenom' value={nom} onChange={(e) => setNom(e.target.value)} type="text" name='user_name' required />
        </div>

        <div className="input_Contact">
          <input placeholder='Telephone' value={tele} onChange={(e) => setTele(e.target.value)} type="text" name='user_tele' required /> {/* تغيير نوع الحقل وتحديد القيمة لهاتف */}
        </div>

        <div className="input_Contact">
          <input placeholder=' Email' value={email} onChange={(e) => setEmail(e.target.value)} type="text" name='user_email' required />
        </div>
        <div className="input_Contact">

          <textarea placeholder='Votre Message' value={theme} onChange={(e) => setTheme(e.target.value)} id='message' name='message' rows={4} required />
        </div>
        <div className='experienceTop'>
      <div className="containerEX">
        </div>
        </div>
        <button className='buttonSubmit' type='submit'>Send</button>
      </form>
    </div>
  );
}

export default Contact;