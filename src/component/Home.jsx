import React from 'react';
import Products from './Products';

const Home = () => {
    return (
        <div className='hero'>
            <div className="card text-bg-dark text-white border-0">
                <img src="/assets/bg44.jpg" className="card-img" alt="Background" height="550px" />
                <div className="card-img-overlay d-flex flex-column justify-content-center">
                    <div className="container">
                    <h5 className="card-title display-3 fw-bolder mb-0" style={{fontSize:'60px'}}><em></em></h5>
                   
                </div>
                </div>
            </div>
         <Products/>
         </div>
       
    );


}
export default Home; 