import React from 'react';
import { useSelector, useDispatch } from 'react-redux';
import { delCart, addCart } from '../redux/action';
import './Cart.css'

const Cart = () => {
  const cartItems = useSelector((state) => state.handleCart);
  const dispatch = useDispatch();

  const handleRemove = (product) => {
    dispatch(delCart(product));
  };

  const handleAdd = (product) => {
    dispatch(addCart(product));
  };

  return (
    
    <div className='cart1'>
      {cartItems.length === 0 ? (
        <div className='text-center'>
          <h3>Your Cart is Empty</h3>
        </div>
      ) : (
        <div id='card2'>
          {cartItems.map((product) => (
            <div key={product.id} className="row" >
              <div className=" container-fluid col-md-4 cart2">
                {product.img && (
                  <img src={product.img} alt={product.title} height="200px" width="200px" style={{borderRadius:'10px'}}/>
                )}
              </div>
              <div className="col-md-6">
                <h3>{product.title}</h3>
                <p className="lead fw-bold"style={{height:'auto'}}>
                  {product.qty} x ${product.price} = ${product.qty * product.price}
                </p>
                <button
                  className="btn btn-outline-dark me-4"
                  onClick={() => handleRemove(product)}
                >
                  <i className="fa fa-minus"></i>
                </button>
                <button
                  className="btn btn-outline-dark"
                  onClick={() => handleAdd(product)}
                >
                  <i className="fa fa-plus"></i>
                </button>
              </div>
            </div>
          ))}
        </div>
      )}
    </div>
  );
};

export default Cart;