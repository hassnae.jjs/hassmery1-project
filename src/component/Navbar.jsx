import React from 'react';
import { NavLink } from 'react-router-dom';
import { useSelector } from 'react-redux';
import styled from 'styled-components';

const Navbar = () => {
    const state = useSelector((state) => state.handleCart);

    const StyledNavbarContainer = styled.div`
        .navbar {
            background-color: #fff;
            padding: 15px 0;
            width:99%;
            background-color: rgba(255, 255, 255, 0.5);
            transition: transform 0.3s ease, box-shadow 0.3s ease;
            }
              
  .navbar:hover {
                transform: scale(1.02);
                box-shadow: 0 0 20px rgba(255, 255, 255, 0.7);
              }

  

        .container {
            display: flex;
            justify-content: space-between;
            align-items: center;
        }

        .navbar-brand {
            font-weight: bold;
            font-size: 1.5rem;
            text-decoration: none;
            color: #333;
        }

        .navbar-toggler {
            display: none;
            background-color: transparent;
            border: none;
        }

        .navbar-collapse {
            display: flex;
            align-items: center;
        }

        .navbar-nav {
            list-style: none;
            display: flex;
            margin: 0;
            padding: 0;
        }

        .nav-item {
            margin-right: 20px;
        }

        .nav-link {
            text-decoration: none;
            color: #333;
            font-weight: 500;
            font-size: 1rem;
            transition: color 0.3s ease;
        }

        .nav-link:hover {
            color: rgb(148, 71, 71);
        }

        .buttons {
            display: flex;
            align-items: center;
        }

        .btn {
            text-decoration: none;
            padding: 8px 16px;
            margin-right: 10px;
            border: 1px solid rgb(223, 150, 150);
            color :rgb(223, 150, 150);
            border-radius: 4px;
            cursor: pointer;
            transition: background-color 0.3s ease, color 0.3s ease;
        }

        .btn:hover {
            background-color: rgb(223, 150, 150);
            color: white;
        }

        @media (max-width: 768px) {
            .navbar-toggler {
                display: block;
                cursor: pointer;
            }

            .navbar-collapse {
                display: none;
                flex-direction: column;
                width: 100%;
                position: absolute;
                top: 60px;
                left: 0;
                background-color: #fff;
                box-shadow: 0 1px 3px rgba(0, 0, 0, 0.12), 0 1px 2px rgba(0, 0, 0, 0.24);
            }

            .navbar-nav {
                flex-direction: column;
            }

            .nav-item {
                margin-right: 0;
                margin-bottom: 10px;
            }

            .navbar-toggler.active + .navbar-collapse {
                display: flex;
            }
        }
    `;

    return (
        <StyledNavbarContainer>
            <nav className="navbar navbar-expand-lg navbar-light bg-white py-3 shadow-sm">
                <div className="container">
                   <div className='hand'><NavLink className="navbar-brand fw-bold fs-4" to="/"><em>H A S S M E R Y</em></NavLink></div>
                    <button className="navbar-toggler" type="button" data-bs-toggle="collapse" data-bs-target="#navbarSupportedContent" aria-controls="navbarSupportedContent" aria-expanded="false" aria-label="Toggle navigation">
                        <span className="navbar-toggler-icon"></span>
                    </button>
                    <div className="collapse navbar-collapse" id="navbarSupportedContent">
                        <ul className="navbar-nav mx-auto mb-2 mb-lg-0">
                            <li className="nav-item">
                                <NavLink className="nav-link active" aria-current="page" to="/">Home</NavLink>
                            </li>
                            <li className="nav-item">
                                <NavLink className="nav-link" to="/products">Product</NavLink>
                            </li>
                            <li className="nav-item">
                                <NavLink className="nav-link" to="/about">About</NavLink>
                            </li>
                            <li className="nav-item">
                                <NavLink className="nav-link" to="/contact">Contact</NavLink>
                            </li>
                        </ul>
                        <div className='buttons'>
                            <NavLink to="/login" className="btn btn ">
                                <i className="fa fa-sign-in me-1"></i> Login
                            </NavLink>
                            <NavLink to="/register" className="btn btn ms-2">
                                <i className="fa fa-user-plus me-1 "></i> Register
                            </NavLink>
                            <NavLink to="/cart" className="btn  ms-2">
                                <i className="fa fa-shopping-cart me-1 "></i> Cart({state.length})
                            </NavLink>
                        </div>
                    </div>
                </div>
            </nav>
        </StyledNavbarContainer>
    );
}

export default Navbar;
