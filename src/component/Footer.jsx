import React from 'react'
import './Footer.css'
function Footer() {
  return (
    <div className='footer'> 
    <div className="container-footer">
      <div className="row-footer">
        <div className="footer-col">
          <h4>H A S S M E R Y </h4>
          <ul>
            <li><a href="/products">Products</a></li>
            <li><a href="/register">Register</a></li>
            <li><a href="/login">Login</a></li>
          </ul>
        </div>
        <div className="footer-col">
          <h4>you need Help?</h4>
          <ul>
            <li><a href="/contact">Contact</a></li>
            <li><a href="/about">About us</a></li>
            <li><a href="/home">Retourne</a></li>
          </ul>
        </div>
        <div className="footer-col">
          <h4><em>About</em></h4>
          <ul>
            <li><a href="#">Email : hassmery@gmail.com</a></li>
            <li><a href="#">Phone : +2120252268128574</a></li>
          </ul>
        </div>
      </div>
    </div>
      

    </div>
  )
}
export default Footer