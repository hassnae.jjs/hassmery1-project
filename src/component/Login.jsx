import React, { useEffect, useState } from "react";
import { toast } from "react-toastify";
import { useNavigate } from "react-router-dom";
import './Login.css'

const Login = () => {
  const [username, setUsername] = useState('');
  const [password, setPassword] = useState('');
  const navigate = useNavigate();

  useEffect(() => {
    sessionStorage.clear();
  }, []);

  const ProceedLogin = (e) => {
    e.preventDefault();
    if (validate()) {
      fetch("http://localhost:8000/user/" + username)
        .then((res) => res.json())
        .then((resp) => {
          if (Object.keys(resp).length === 0) {
            toast.error('Please Enter a valid username');
          } else {
            if (resp.password === password) {
              toast.success('Login successful');
              sessionStorage.setItem('username', username);
              sessionStorage.setItem('userrole', resp.role);
              navigate('/products');
            } else {
              toast.error('Please Enter valid credentials');
            }
          }
        })
        .catch((err) => {
          toast.error('Login Failed due to :' + err.message);
        });
    }
  };

  const validate = () => {
    let result = true;
    if (username === '' || username === null) {
      result = false;
      toast.warning('Please Enter Username');
    }
    if (password === '' || password === null) {
      result = false;
      toast.warning('Please Enter Password');
    }
    return result;
  };

  return (
    <div class=" logintop">
    <div class=" logintop2" >
        <form onSubmit={ProceedLogin} class="custom-container">
            <div class="custom-card logintop3">
                <div class="custom-card-header logintop4">
                    <h2 class="custom-container logintop5">Log In</h2>
                </div>
                <div class="custom-card-body logintop6">
                    <div class="custom-form-group">
                        <label>User Name <span class="custom-errmsg">*</span></label>
                        <input
                            value={username}
                            onChange={(e) => setUsername(e.target.value)}
                            class="custom-form-control logintop7"
                        />
                    </div>
                    <div class="custom-form-group logintop8">
                        <label>Password <span class="custom-errmsg">*</span></label>
                        <input
                            type="password"
                            value={password}
                            onChange={(e) => setPassword(e.target.value)}
                            class="custom-form-control log"
                        />
                    </div>
                </div>
                <div class="custom-card-footer logingtop9">
                    <button type="submit" class="logintop10">
                        Login
                    </button>
                </div>
            </div>
        </form>
    </div>
</div>

  );
};

export default Login;
