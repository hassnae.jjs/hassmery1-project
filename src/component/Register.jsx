import { useState } from "react";
import { useNavigate } from "react-router";
import { toast } from "react-toastify";
import './Register.css'
const Register=()=>{
    const [name,namechange]=useState("");
    const [password,passwordchange]=useState("");
    const [email,emailchange]=useState("");


    const navigate=useNavigate();
    const handleBack = () => {
        navigate(-1);
      };
    const IsValidate=()=>{
        let isproceed=true;
        return isproceed;
    }

    const handlesubmit = (e) =>{
        if(IsValidate()){
        e.preventDefault();
        let regobj={name,password,email};
        fetch("http://localhost:8000/user",{
            method:"POST",
            headers:{'content-type':'application/json'},
            body:JSON.stringify(regobj)

        }).then((res)=>{
            toast.success('Registred successfully.')
            navigate('/Login')
        }).catch((err)=>{
            toast.error('failed :'+err.message)
        });}
    }

    return(
<div>
    <div class="custom-offset-lg-3 custom-col-6 custom-registerTop">
        <form class="custom-container" onSubmit={handlesubmit}>
            <div class="custom-card">
                <div class="custom-card-header">
                    <h1 class="custom-container">Sign Up</h1>
                </div>
                <div class="custom-card-body">
                    <div class="custom-row">
                       <div class="custom-col-1g-6">
                            <div class="custom-form-group">
                                <label>Full Name : <span class="custom-errmsg">*</span></label>
                                <input value={name} onChange={e=>namechange(e.target.value)} class="custom-form-control"></input>
                            </div>
                        </div>
                        <div class="custom-col-1g-6">
                            <div class="custom-form-group">
                                <label>Email : <span class="custom-errmsg">*</span></label>
                                <input  value={email} onChange={e=>emailchange(e.target.value)} class="custom-form-control"></input>
                            </div>
                        </div>
                        <div class="custom-col-1g-6">
                            <div class="custom-form-group">
                                <label>Password : <span class="custom-errmsg">*</span></label>
                                <input type="password" value={password} onChange={e=>passwordchange(e.target.value)} class="custom-form-control"></input>
                            </div>
                        </div>
                    </div>
                </div>

                <div class="custom-card-footer">
                    <button type="submit" onClick={handlesubmit} class="custom-btn custom-btn-primary">Register</button><br/> <br/> 
                    <a class="custom-btn custom-btn-danger" onClick={handleBack}>Back</a>
                </div>
            </div>
        </form>
    </div>
</div>

    )
}



export default Register;